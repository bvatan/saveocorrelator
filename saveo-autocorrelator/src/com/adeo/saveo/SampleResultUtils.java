package com.adeo.saveo;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.config.Argument;
import org.apache.jmeter.protocol.http.config.MultipartUrlConfig;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.util.HTTPConstants;
import org.apache.jmeter.testelement.property.JMeterProperty;
import org.apache.jmeter.util.JMeterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SampleResultUtils {

	private static final Logger log = LoggerFactory.getLogger(SampleResultUtils.class);
	
	private static final String PARAM_CONCATENATE = "&";

	private static final String CHARSET_DECODE = StandardCharsets.ISO_8859_1.name();

	private static boolean isMultipart(LinkedHashMap<String, String> headers) {
		String contentType = headers.get(HTTPConstants.HEADER_CONTENT_TYPE);
		return contentType != null && contentType.startsWith(HTTPConstants.MULTIPART_FORM_DATA);
	}

	public static Map<String, List<String>> parseArguments(HTTPSampleResult sampleResult) {
		Map<String, List<String>> arguments = new LinkedHashMap<>();
		URL hUrl = sampleResult.getURL();
		String queryGet = hUrl.getQuery() == null ? "" : hUrl.getQuery();
		LinkedHashMap<String, String> lhm = JMeterUtils.parseHeaders(sampleResult.getRequestHeaders());
		boolean isMultipart = isMultipart(lhm);
		// Concatenate query post if exists
		String queryPost = sampleResult.getQueryString();
		if (!isMultipart && StringUtils.isNotBlank(queryPost)) {
			if (queryGet.length() > 0) {
				queryGet += PARAM_CONCATENATE;
			}
			queryGet += queryPost;
		}

		if (StringUtils.isNotBlank(queryGet)) {
			Set<Entry<String, String[]>> keys = getQueryMap(queryGet).entrySet();
			for (Entry<String, String[]> entry : keys) {
				arguments.put(entry.getKey(), Arrays.asList(entry.getValue()));
			}
		}

		if (isMultipart && StringUtils.isNotBlank(queryPost)) {
			String contentType = lhm.get(HTTPConstants.HEADER_CONTENT_TYPE);
			String boundaryString = extractBoundary(contentType);
			MultipartUrlConfig urlconfig = new MultipartUrlConfig(boundaryString);
			urlconfig.parseArguments(queryPost);

			for (JMeterProperty prop : urlconfig.getArguments()) {
				Argument arg = (Argument) prop.getObjectValue();
				List<String> values = arguments.get(arg.getName());
				if(values == null) {
					values = new LinkedList<>();
					arguments.put(arg.getName(), values);
				}
				values.add(arg.getValue());
			}
		}
		return arguments;
	}

	private static String extractBoundary(String contentType) {
		// Get the boundary string for the multiparts from the content type
		String boundaryString = contentType.substring(contentType.toLowerCase(java.util.Locale.ENGLISH).indexOf("boundary=") + "boundary=".length());
		// TODO check in the RFC if other char can be used as separator
		String[] split = boundaryString.split(";");
		if (split.length > 1) {
			boundaryString = split[0];
		}
		return boundaryString;
	}

	public static Map<String, String[]> getQueryMap(String query) {

		Map<String, String[]> map = new HashMap<>();
		String[] params = query.split(PARAM_CONCATENATE);
		for (String param : params) {
			String[] paramSplit = param.split("=");
			String name = decodeQuery(paramSplit[0]);

			// hack for SOAP request (generally)
			if (name.trim().startsWith("<?")) { // $NON-NLS-1$
				map.put(" ", new String[] { query }); // blank name // $NON-NLS-1$
				return map;
			}

			// the post payload is not key=value
			if ((param.startsWith("=") && paramSplit.length == 1) || paramSplit.length > 2) {
				map.put(" ", new String[] { query }); // blank name // $NON-NLS-1$
				return map;
			}

			String value = "";
			if (paramSplit.length > 1) {
				value = decodeQuery(paramSplit[1]);
			}

			String[] known = map.get(name);
			if (known == null) {
				known = new String[] { value };
			} else {
				String[] tmp = new String[known.length + 1];
				tmp[tmp.length - 1] = value;
				System.arraycopy(known, 0, tmp, 0, known.length);
				known = tmp;
			}
			map.put(name, known);
		}

		return map;
	}

	public static String decodeQuery(String query) {
		if (query != null && query.length() > 0) {
			try {
				return URLDecoder.decode(query, CHARSET_DECODE); // better ISO-8859-1 than UTF-8
			} catch (IllegalArgumentException | UnsupportedEncodingException e) {
				log.warn("Error decoding query, maybe your request parameters should be encoded:" + query, e);
				return query;
			}
		}
		return "";
	}

}
