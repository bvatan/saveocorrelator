package com.adeo.saveo.rules;

import java.util.regex.Pattern;

public class RegexRule {

	private Pattern pattern;
	private int position;
	private String destName;
	private boolean singleValued;

	public RegexRule(String pattern) {
		this.pattern = Pattern.compile(pattern);
	}
	
	public RegexRule(String pattern, int position, String destName) {
		this.pattern = Pattern.compile(pattern);
		this.position = position;
		this.destName = destName;
		this.singleValued = true;
	}
	
	public boolean isSingleValued() {
		return singleValued;
	}
	
	public Pattern getPattern() {
		return pattern;
	}
	
	public int getPosition() {
		return position;
	}
	
	public String getVariableName() {
		return destName;
	}
}
