package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class JsonA209XRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(JsonA209XRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*action$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_OID = "$lbp$oid";

	private final String[][] JSON_RULES = {
			{"$[*][?(@.eltID=='h_id_rule_accessoires')].value[*].value" , "h_id_rule_accessoires"},
			{"$[*][?(@.eltID=='h_id_rule_savcodes')].value[*].value" , "h_id_rule_savcodes"},
			{"$[*][?(@.eltID=='h_chk_gestion_rebonds')].value[*].value" , "h_chk_gestion_rebonds"},
			{"$[*][?(@.eltID=='h_mode_acces_sernum1')].value[*].value" , "h_mode_acces_sernum1"},
			{"$[*][?(@.eltID=='h_mode_acces_sernum2')].value[*].value" , "h_mode_acces_sernum2"},
			{"$[*][?(@.eltID=='h_mode_acces_sympt')].value[*].value" , "h_mode_acces_sympt"},
			{"$[*][?(@.eltID=='symptom1')].value[*].value" , "symptom1"},
			{"$[*][?(@.eltID=='h_mode_acces_cond')].value[*].value" , "h_mode_acces_cond"},
			{"$[*][?(@.eltID=='defauts_client')].value[*].value" , "defauts_client"},
			{"$[*][?(@.eltID=='symptom1')].value[*].value" , "symptom1"},
			{"$[*][?(@.eltID=='status_main')].value[*].value" , "status_main"},
			{"$[*][?(@.eltID=='h_idrules_piece_integrees')].value[*].value" , "h_idrules_piece_integrees"},
			{"$[*][?(@.eltID=='sous_traitant')].value[*].value" , "sous_traitant"},
			{"$[*][?(@.eltID=='h_id_rule_st')].value[*].value" , "h_id_rule_st"},
	};
	
	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "A209".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_OID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String masterFrameKey = "frame:" + samplerArgs.get(ARG_FRAME_EXEC_ID);
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		if (frameVars != null) {
			String encodedJson = ExtractorUtils.extractWithRegex(httpSampleResult.getResponseDataAsString(), PATTERN_ENCODED_JSON, 1);
			String json = StringEscapeUtils.unescapeHtml(encodedJson);
			
			for(String[] rule : JSON_RULES) {
				ExtractorUtils.extractFirstJsonPath(json, rule[0], frameVars, rule[1]);
			}
			String threadGroupName = JMeterContextService.getContext().getThreadGroup().getName();
			if(threadGroupName.startsWith("S10")) {
				frameVars.remove("status_main");
			}
			
		} else {
			if (log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} !", samplerArgs.get(ARG_FRAME_EXEC_ID), samplerArgs.get(ARG_FID));
			}
		}
	}

}
