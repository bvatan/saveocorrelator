package com.adeo.saveo.rules;

import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;

import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

abstract public class Rule {
	
	public String getId() {
		return getClass().getSimpleName();
	}

	abstract public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables);
	
	public void applyBefore(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		
	}
	
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		
	}
}
