package com.adeo.saveo.rules;

import java.util.regex.Pattern;

import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class FrameA1_A2146XRule extends Rule {

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*frame$");

	private static final String ARG_FID = "$lbp$fid";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "LBS01_A1_A2146".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_FID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameIdKey = "originatingFrame:" + samplerBase.getComment();
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(frameIdKey);
		if (frameVars != null) {
			Document document = Jsoup.parse(httpSampleResult.getResponseDataAsString());
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_notif_relance_devis_def_report", 0, "value", frameVars, "h_notif_relance_devis_def_report");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_type_interloc_relance_FINREP", 0, "value", frameVars, "h_id_type_interloc_relance_FINREP");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_notif_finrep_def_report", 0, "value", frameVars, "h_notif_finrep_def_report");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_type_interloc_relance_DEVIS_R", 0, "value", frameVars, "h_id_type_interloc_relance_DEVIS_R");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_notif_devis_def_report", 0, "value", frameVars, "h_notif_devis_def_report");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_type_interloc_relance_DEVIS", 0, "value", frameVars, "h_id_type_interloc_relance_DEVIS");
			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_block_level", 0, "value", frameVars, "h_block_level");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_condition", 0, "value", frameVars, "h_id_savcodetype_condition");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_symptome", 0, "value", frameVars, "h_id_savcodetype_symptome");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_choice", 0, "value", frameVars, "h_id_savcodetype_choice");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_cumul_accessoire", 0, "value", frameVars, "h_id_cumul_accessoire");
//			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_const_max_count_savcodes", 0, "value", frameVars, "h_const_max_count_savcodes");
			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_attache_table", 0, "value", frameVars, "h_attache_table");
		}
	}

}
