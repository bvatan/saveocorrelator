package com.adeo.saveo.rules;

import java.util.regex.Pattern;

import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class FrameA1_A2469XRule extends Rule {

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*frame$");

	private static final String ARG_FID = "$lbp$fid";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "LBS01_A1_A2469".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_FID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameIdKey = "originatingFrame:" + samplerBase.getComment();
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(frameIdKey);
		if (frameVars != null) {
			Document document = Jsoup.parse(httpSampleResult.getResponseDataAsString());
			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_current_status", 0, "value", frameVars, "status_main");
			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_code_article_c03", 0, "value", frameVars, "h_code_article_c03");
		}
	}

}
