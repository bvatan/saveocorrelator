package com.adeo.saveo.rules;

import java.util.regex.Pattern;

import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterContext;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

import groovy.json.StringEscapeUtils;

public class FrameA1_A2131XRule extends Rule {

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*frame$");

	private static final Pattern PATTERN_DATE_ACHAT = Pattern.compile("LBPDateField\\('date_achat'[^']+'([^']*)'");

	private static final String ARG_FID = "$lbp$fid";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "LBS01_A1_A2131".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_FID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameIdKey = "originatingFrame:" + samplerBase.getComment();
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(frameIdKey);
		if (frameVars != null) {
			Document document = Jsoup.parse(httpSampleResult.getResponseDataAsString());
			String threadGroupName = JMeterContextService.getContext().getThreadGroup().getName();
			if (threadGroupName.startsWith("S04")) {
				// added for ouverture dossier
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_mag", 0, "value", frameVars, "h_id_mag");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_addr_livr_main", 0, "value", frameVars, "h_id_addr_livr_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_client_main", 0, "value", frameVars, "h_client_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_condition", 0, "value", frameVars, "h_id_savcodetype_condition");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_symptome", 0, "value", frameVars, "h_id_savcodetype_symptome");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_type_interloc_relance_FINREP", 0, "value", frameVars, "h_id_type_interloc_relance_FINREP");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_litigation", 0, "value", frameVars, "h_id_savcodetype_litigation");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_frequency", 0, "value", frameVars, "h_id_savcodetype_frequency");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_notification", 0, "value", frameVars, "h_id_savcodetype_notification");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_choice", 0, "value", frameVars, "h_id_savcodetype_choice");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_symptome", 0, "value", frameVars, "h_id_savcodetype_symptome");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_article_main", 0, "value", frameVars, "h_id_article_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_datec_trunc_main", 0, "value", frameVars, "h_datec_trunc_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_famille1article_main", 0, "value", frameVars, "h_id_famille1article_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_famille2article_main", 0, "value", frameVars, "h_id_famille2article_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_famille3article_main", 0, "value", frameVars, "h_id_famille3article_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_famille4article_main", 0, "value", frameVars, "h_id_famille4article_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_client_main", 0, "value", frameVars, "h_client_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_famille_client_main", 0, "value", frameVars, "h_famille_client_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_group_client_main", 0, "value", frameVars, "h_group_client_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#type_interv", 0, "value", frameVars, "type_interv");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_garantie_reel", 0, "value", frameVars, "h_id_garantie_reel");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_garantie_main", 0, "value", frameVars, "h_id_garantie_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_rais_garantie_reel", 0, "value", frameVars, "h_id_rais_garantie_reel");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_rais_garantie_main", 0, "value", frameVars, "h_id_rais_garantie_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_garantie_main_loaded", 0, "value", frameVars, "h_id_garantie_main_loaded");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_rais_garantie_main_loaded", 0, "value", frameVars, "h_id_rais_garantie_main_loaded");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_article_main_prix_ach", 0, "value", frameVars, "h_id_article_main_prix_ach");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_article_main_prix_vte_ttc", 0, "value", frameVars, "h_id_article_main_prix_vte_ttc");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_depot_main", 0, "value", frameVars, "h_id_depot_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_type_interv_sav", 0, "value", frameVars, "h_id_type_interv_sav");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_cumul_accessoire", 0, "value", frameVars, "h_id_cumul_accessoire");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_condition", 0, "value", frameVars, "h_id_savcodetype_condition");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_poids_article_main", 0, "value", frameVars, "h_poids_article_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_numero_facture_achat", 0, "value", frameVars, "h_numero_facture_achat");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_datec_main", 0, "value", frameVars, "h_datec_main");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_ETAT_ACHATS", 0, "value", frameVars, "h_id_ETAT_ACHATS");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_ETAT_VENTES", 0, "value", frameVars, "h_id_ETAT_VENTES");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_attache_table", 0, "value", frameVars, "h_attache_table");
				String dateAchat = ExtractorUtils.extractWithRegex(httpSampleResult.getResponseDataAsString(), PATTERN_DATE_ACHAT, 1);
				frameVars.put("date_achat", StringEscapeUtils.unescapeJava(dateAchat));
				String ticketNo = frameVars.get("h_numero_facture_achat");
				frameVars.put("h_article_last_hist_produit", frameVars.get("h_id_article_main"));
				frameVars.put("h_client_last_hist_produit", frameVars.get("h_client_main"));
				frameVars.put("sernum1", ticketNo);
				frameVars.put("h_sernum1_last_hist_produit", ticketNo);
				frameVars.put("h_attache_idrecord", frameVars.get("h_siglnum_main"));
				String[] ticketTokens = ticketNo == null ? new String[] {"",""} : ticketNo.split("-");
				frameVars.put("magasin", ticketTokens[0]);
				frameVars.put("ticketNo", ticketTokens[1]);
				// pour sampler monkey et son action sur h_numero_facture_achat
				variables.put("magasin", ticketTokens[0]);
				variables.put("ticketNo", ticketTokens[1]);
			} else {
				// added for demarque qualite
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_notif_relance_devis_def_report", 0, "value", frameVars, "h_notif_relance_devis_def_report");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_type_interloc_relance_FINREP", 0, "value", frameVars, "h_id_type_interloc_relance_FINREP");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_notif_finrep_def_report", 0, "value", frameVars, "h_notif_finrep_def_report");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_type_interloc_relance_DEVIS_R", 0, "value", frameVars, "h_id_type_interloc_relance_DEVIS_R");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_notif_devis_def_report", 0, "value", frameVars, "h_notif_devis_def_report");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_type_interloc_relance_DEVIS", 0, "value", frameVars, "h_id_type_interloc_relance_DEVIS");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_block_level", 0, "value", frameVars, "h_block_level");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_condition", 0, "value", frameVars, "h_id_savcodetype_condition");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_symptome", 0, "value", frameVars, "h_id_savcodetype_symptome");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_savcodetype_choice", 0, "value", frameVars, "h_id_savcodetype_choice");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_cumul_accessoire", 0, "value", frameVars, "h_id_cumul_accessoire");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_const_max_count_savcodes", 0, "value", frameVars, "h_const_max_count_savcodes");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_attache_table", 0, "value", frameVars, "h_attache_table");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_gar_NDEF", 0, "value", frameVars, "h_gar_NDEF");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_status_NDEF", 0, "value", frameVars, "h_id_status_NDEF");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_actionstamp", 0, "value", frameVars, "h_actionstamp");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_ETAT_ACHATS", 0, "value", frameVars, "h_id_ETAT_ACHATS");
				ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_id_ETAT_VENTES", 0, "value", frameVars, "h_id_ETAT_VENTES");
			}
		}
	}

}
