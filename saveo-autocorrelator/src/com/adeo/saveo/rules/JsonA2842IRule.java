package com.adeo.saveo.rules;

import java.util.regex.Pattern;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.protocol.http.util.HTTPArgument;
import org.apache.jmeter.testelement.property.JMeterProperty;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;

import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class JsonA2842IRule extends Rule {

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*action$");
	
	private static final String ARG_OID = "$lbp$oid";
	
	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String threadGroupName = JMeterContextService.getContext().getThreadGroup().getName();
		boolean condition = "A2842".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_OID));
		condition = condition && threadGroupName.startsWith("S10");
		condition = condition && MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches();
		return condition;
	}
	
	@Override
	public void applyBefore(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		Arguments arguments = samplerBase.getArguments();
		samplerBase.setArguments(new Arguments());
		for (JMeterProperty argument : arguments) {
			HTTPArgument httpArg = (HTTPArgument) argument.getObjectValue();
			if("modeexp".equals(argument.getName())) {
				samplerBase.getArguments().addArgument(new HTTPArgument(httpArg.getName(), "", httpArg.getMetaData()));
			} else {
				samplerBase.getArguments().addArgument(httpArg);
			}
		}
	}

}
