package com.adeo.saveo.rules;

import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;

import com.adeo.saveo.ExtractorUtils;
import com.adeo.saveo.SaveoFrame;
import com.jayway.jsonpath.JsonPath;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class FrameXRule extends Rule {

	private static final Pattern PATTERN_FRAME_EXEC_ID = Pattern.compile("\\$LBP\\$FRAME_EXECUTION_CONTEXT_ID='([^']*)'");

	private static final Pattern PATTERN_AAT = Pattern.compile("lbp.vars.aat='([^']+)'");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*frame$");

	private static final Pattern PATTERN_FAT_VARS = Pattern.compile("lbp.vars.fat=([^;]*)");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_AAT = "$lbp$aat";
	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_LBP_GDT_GADGET_ID = "$lbp$gdt$gadgetID";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches();
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameResponseContent = httpSampleResult.getResponseDataAsString();
		SaveoFrame frameVars = new SaveoFrame();
		ExtractorUtils.extractWithRegex(frameResponseContent, PATTERN_FRAME_EXEC_ID, 1, frameVars, ARG_FRAME_EXEC_ID);
		ExtractorUtils.extractWithRegex(frameResponseContent, PATTERN_AAT, 1, frameVars, ARG_AAT);
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String frameKey = "frame:" + frameVars.get(ARG_FRAME_EXEC_ID);
		variables.putObject(frameKey, frameVars);
		String frameOriginIdKey = "originatingFrame:" + samplerBase.getComment();
		variables.putObject(frameOriginIdKey, frameVars);
		String frameIdKey = "frameId:" + samplerArgs.get(ARG_FID);
		variables.putObject(frameIdKey, frameVars);
//		String origininatingFrameExecId = samplerArgs.get(ARG_FRAME_EXEC_ID);
//		if (origininatingFrameExecId != null) {
//			variables.get("frame:" + origininatingFrameExecId);
//			frameVars.addParentFrame((SaveoFrame) variables.getObject("frame:" + origininatingFrameExecId));
//		}
		frameVars.setParentFrame((SaveoFrame)variables.getObject("lastFrameVariables"));
		variables.remove("lastFrameVariables");
		// fat tokens indexed by frame ids (ex. LBS01_A1_A2273=5_SaxJmroStsP-w_vNH8WQ)
		String jsonFatTokens = ExtractorUtils.extractWithRegex(frameResponseContent, PATTERN_FAT_VARS, 1);
		if (!StringUtils.isBlank(jsonFatTokens)) {
			Map<String, String> fatMap = JsonPath.read(jsonFatTokens, "$");
			for (Entry<String, String> frameEntry : fatMap.entrySet()) {
				String frameId = frameEntry.getKey();
				String frameFatToken = frameEntry.getValue();
				// ex. frameId=LBS01_A1_A2273, frameFatToken=5_SaxJmroStsP-w_vNH8WQ
				frameVars.put("frameFat:" + frameId, frameFatToken);
				variables.putObject(frameId + ":" + frameFatToken, frameVars);
			}
		}
		String frameId = samplerArgs.get(ARG_FID);
		if ("LBS01_A1_A2612".equals(frameId)) {
			String gadgetId = samplerArgs.get(ARG_LBP_GDT_GADGET_ID);
			variables.putObject(frameId + ":" + gadgetId, frameVars);
		}

	}

}
