package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class JsonA1077XRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(JsonA1077XRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*action$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_OID = "$lbp$oid";
	
	private static final String JSON_PATH_H_ID_ARTICLE_MAIN = "$[*][?(@.eltID=='h_id_article_main')].value[*].value";
	private static final String JSON_PATH_H_ID_FAMILLE1ARTICLE_MAIN = "$[*][?(@.eltID=='h_id_famille1article_main')].value[*].value";
	private static final String JSON_PATH_H_ID_FAMILLE2ARTICLE_MAIN = "$[*][?(@.eltID=='h_id_famille2article_main')].value[*].value";
	private static final String JSON_PATH_H_ID_FAMILLE3ARTICLE_MAIN = "$[*][?(@.eltID=='h_id_famille3article_main')].value[*].value";
	private static final String JSON_PATH_H_ID_FAMILLE4ARTICLE_MAIN = "$[*][?(@.eltID=='h_id_famille4article_main')].value[*].value";
	private static final String JSON_PATH_H_ID_ARTICLE_MAIN_PRIX_ACH = "$[*][?(@.eltID=='h_id_article_main_prix_ach')].value[*].value";
	private static final String JSON_PATH_H_ID_ARTICLE_MAIN_PRIX_VTE_TTC = "$[*][?(@.eltID=='h_id_article_main_prix_vte_ttc')].value[*].value";
	private static final String JSON_PATH_H_ID_ARTICLE_MAIN_PRIX_VTE = "$[*][?(@.eltID=='h_id_article_main_prix_vte')].value[*].value";
	private static final String JSON_PATH_H_POIDS_ARTICLE_MAIN = "$[*][?(@.eltID=='h_poids_article_main')].value[*].value";
	
	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "A1077".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_OID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String masterFrameKey = "frame:" + samplerArgs.get(ARG_FRAME_EXEC_ID);
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		if (frameVars != null) {
			String encodedJson = ExtractorUtils.extractWithRegex(httpSampleResult.getResponseDataAsString(), PATTERN_ENCODED_JSON, 1);
			String json = StringEscapeUtils.unescapeHtml(encodedJson);
			ExtractorUtils.extractFirstJsonPath(json, JSON_PATH_H_ID_ARTICLE_MAIN, frameVars, "h_id_article_main");
			ExtractorUtils.extractFirstJsonPath(json, JSON_PATH_H_ID_FAMILLE1ARTICLE_MAIN, frameVars, "h_id_famille1article_main");
			ExtractorUtils.extractFirstJsonPath(json, JSON_PATH_H_ID_FAMILLE2ARTICLE_MAIN, frameVars, "h_id_famille2article_main");
			ExtractorUtils.extractFirstJsonPath(json, JSON_PATH_H_ID_FAMILLE3ARTICLE_MAIN, frameVars, "h_id_famille3article_main");
			ExtractorUtils.extractFirstJsonPath(json, JSON_PATH_H_ID_FAMILLE4ARTICLE_MAIN, frameVars, "h_id_famille4article_main");
			ExtractorUtils.extractFirstJsonPath(json, JSON_PATH_H_ID_ARTICLE_MAIN_PRIX_ACH, frameVars, "h_id_article_main_prix_ach");
			ExtractorUtils.extractFirstJsonPath(json, JSON_PATH_H_ID_ARTICLE_MAIN_PRIX_VTE_TTC, frameVars, "h_id_article_main_prix_vte_ttc");
			ExtractorUtils.extractFirstJsonPath(json, JSON_PATH_H_ID_ARTICLE_MAIN_PRIX_VTE, frameVars, "h_id_article_main_prix_vte");
			ExtractorUtils.extractFirstJsonPath(json, JSON_PATH_H_POIDS_ARTICLE_MAIN, frameVars, "h_poids_article_main");
		} else {
			if(log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} !", samplerArgs.get(ARG_FRAME_EXEC_ID), samplerArgs.get(ARG_FID));
			}
		}
	}

}
