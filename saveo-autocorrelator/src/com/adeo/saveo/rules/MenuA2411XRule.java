package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class MenuA2411XRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(MenuA2411XRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern PATTERN_FAT_TOKEN_IN_MENU_URI = Pattern.compile("\\$lbp\\$fat=([^&]+)");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*menu$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";

	private static final String MENU_JSON_PATH = "$[*][?(@.menuID=='%s')].uriEx";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "LBS01_A1_A2411".equals(samplerBase.getArguments().getArgumentsAsMap().get("$lbp$oid"));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String frameExecId = samplerArgs.get(ARG_FRAME_EXEC_ID);
		String masterFrameKey = "frame:" + frameExecId;
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		if (frameVars != null) {
			String encodedJson = ExtractorUtils.extractWithRegex(httpSampleResult.getResponseDataAsString(), PATTERN_ENCODED_JSON, 1);
			String json = StringEscapeUtils.unescapeHtml(encodedJson);
			String menuIds = samplerArgs.get("list");
			for (String menuId : menuIds.trim().split(",")) {
				if(StringUtils.isBlank(menuId)) {
					continue;
				}
				String menuUri = ExtractorUtils.extractFirstJsonPath(json, String.format(MENU_JSON_PATH, menuId));
				if(StringUtils.isBlank(menuUri)) {
					if(log.isDebugEnabled()) {
						log.debug("No menuUri could be extracted for menuId {} from json <{}> with json path <{}>", menuId, json,  String.format(MENU_JSON_PATH, menuId));
					}
				} else {
					ExtractorUtils.extractWithRegex(menuUri, PATTERN_FAT_TOKEN_IN_MENU_URI, 1, frameVars, "menuFat:" + menuId);
				}
			}
		} else {
			if (log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} ! No gadgetsJson will be available !", frameExecId, samplerArgs.get(ARG_FID));
			}
		}
	}

}
