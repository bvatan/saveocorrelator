package com.adeo.saveo.rules;

import java.util.regex.Pattern;

import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class FrameA1_A187XRule extends Rule {

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*frame$");

	private static final String ARG_FID = "$lbp$fid";
	
	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String threadGroupName = JMeterContextService.getContext().getThreadGroup().getName();
		boolean condition = threadGroupName.startsWith("S04");
		condition = condition && MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches();
		condition = condition && "LBS01_A1_A187".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_FID));
		return condition;
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameIdKey = "originatingFrame:" + samplerBase.getComment();
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(frameIdKey);
		if (frameVars != null) {
			Document document = Jsoup.parse(httpSampleResult.getResponseDataAsString());
			// added for ouverture dossier
			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_attache_table", 0, "value", frameVars, "h_attache_table");
			ExtractorUtils.extractAttributeWithCssQuery(document, "INPUT#h_attache_idrecord", 0, "value", frameVars, "h_attache_idrecord");
		}
	}

}
