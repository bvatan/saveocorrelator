package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class GadgetXRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(GadgetXRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*gadget$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "10038".equals(samplerBase.getArguments().getArgumentsAsMap().get("gdb"));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameResponseContent = httpSampleResult.getResponseDataAsString();
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String frameExecId = samplerArgs.get(ARG_FRAME_EXEC_ID);
		String frameId = samplerArgs.get(ARG_FID);
		String masterFrameKey = "frame:" + frameExecId;
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		String encodedJson = ExtractorUtils.extractWithRegex(frameResponseContent, PATTERN_ENCODED_JSON, 1);
		String json = StringEscapeUtils.unescapeHtml(encodedJson);
		correlatedVariables.put("gadgetsJson", json);
		if (frameVars != null) {
			frameVars.put("gadgetsJson", json);
		} else {
			if(log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} ! No gadgetsJson will be available !", frameExecId, frameId);
			}
		}
//		HTTPSampleResult lastRes = httpSampleResult;
//		String path = samplerBase.getPath();
//		SampleResult[] subRes = httpSampleResult.getSubResults();
//		if (subRes != null && subRes.length > 0) {
//			lastRes = (HTTPSampleResult) subRes[subRes.length - 1];
//		}
//		Map<String, List<String>> requestArgs = SampleResultUtils.parseArguments(lastRes);
//		System.out.println("path=" + path + " :" + requestArgs.get("$lbp$fid"));
	}

}
