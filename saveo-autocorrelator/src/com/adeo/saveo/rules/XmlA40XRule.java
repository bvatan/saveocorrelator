package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class XmlA40XRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(XmlA40XRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*xml$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_OID = "$lbp$oid";

	private static final String JSON_PATH_H_ID_TICKET = "$[*][?(@.NO_TICKET=='%s')].LMR_RECEIPT_ID";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "$$A40".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_OID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameResponseContent = httpSampleResult.getResponseDataAsString();
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String masterFrameKey = "frame:" + samplerArgs.get(ARG_FRAME_EXEC_ID);
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		if (frameVars != null) {
			String encodedJson = ExtractorUtils.extractWithRegex(frameResponseContent, PATTERN_ENCODED_JSON, 1);
			String json = StringEscapeUtils.unescapeHtml(encodedJson);

			String ticketNo = variables.get("ticketNo");
			String magasin = variables.get("magasin");

			ExtractorUtils.extractFirstJsonPath(json, String.format(JSON_PATH_H_ID_TICKET, ticketNo), frameVars, "h_ticket_id");
			frameVars.put("h_tmp_ticket", ticketNo);
			frameVars.put("h_tmp_code_magasin", magasin);
			frameVars.put("h_id_ticket", frameVars.get("h_ticket_id"));
			
			correlatedVariables.put("h_tmp_ticket", ticketNo);
			correlatedVariables.put("h_tmp_code_magasin", magasin);
			correlatedVariables.put("h_id_ticket", frameVars.get("h_ticket_id"));
		} else {
			if (log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} !", samplerArgs.get(ARG_FRAME_EXEC_ID), samplerArgs.get(ARG_FID));
			}
		}
	}

}
