package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.adeo.saveo.InjectorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class FrameIRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(FrameIRule.class);

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*frame$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";

	private static final String ARG_OFID = "$lbp$0fid";

	private static final String ARG_FAT = "$lbp$fat";

	private static final String ARG_LBP_GDT_GADGET_ID = "$lbp$gdt$gadgetID";
	
	private static final String ARG_GADGET_ID = "gadgetID";
	
	private static final String ARG_MENU_ID = "$lbp$menu_id";

	private static final String JSON_PATH_FAT = "$[*][?(@.gadgetID==%s)].content.frame.fat";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches();
	}

	@Override
	public void applyBefore(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		CorrelatedVariables frameVars = null;
		String frameId = samplerArgs.get(ARG_FID);
		if ("LBS01_A1_A2512".equals(frameId)) {
			String originatingFrameId = samplerArgs.get(ARG_OFID);
			String gadgetId = samplerArgs.get(ARG_GADGET_ID);
			frameVars = (CorrelatedVariables)variables.getObject(originatingFrameId + ":" + gadgetId);
		}
		if(frameVars == null) {
			String originalFrameExecId = samplerArgs.get(ARG_FRAME_EXEC_ID);
			if (originalFrameExecId != null) {
				frameVars = (CorrelatedVariables) variables.getObject("originatingFrame:" + originalFrameExecId);
			} else {
				String masterFrameId = samplerArgs.get(ARG_OFID);
				frameVars = (CorrelatedVariables) variables.getObject("frameId:" + masterFrameId);
			}
		}
		if (frameVars != null) {
			if (samplerArgs.containsKey(ARG_FAT)) {
				String menuId = samplerArgs.get(ARG_MENU_ID);
				if(menuId != null) {
					frameVars.put(ARG_FAT, frameVars.get("menuFat:" + menuId));
				} else {
					String gadgetId = samplerArgs.get(ARG_LBP_GDT_GADGET_ID);
					if(gadgetId != null) {
						String fat = getFrameFatForGadget(gadgetId, frameVars);
						if (fat != null) {
							frameVars.put(ARG_FAT, fat);
						}
					} else {
						frameVars.put(ARG_FAT, frameVars.get("frameFat:"+samplerArgs.get(ARG_FID)));
					}
				}
			}
			InjectorUtils.replaceInArguments(samplerBase, correlatedVariables);
			InjectorUtils.replaceInArguments(samplerBase, frameVars);
			variables.putObject("lastFrameVariables", frameVars);
		}
	}
	
	private String getFrameFatForGadget(String gadgetId, CorrelatedVariables vars) {
		String gadgetJson = vars.get("gadgetsJson");
		String fatJsonPath = String.format(JSON_PATH_FAT, gadgetId);
		String frameFat = ExtractorUtils.extractFirstJsonPath(gadgetJson, fatJsonPath);
		if (frameFat == null) {
			if (log.isDebugEnabled()) {
				log.debug("Failed to extract {} value for gadgetId {} from gadgetsJson {} !", ARG_FAT, gadgetId, gadgetJson);
			} else if (log.isErrorEnabled()) {
				log.error("Failed to extract {} value for gadgetId {} from gadgetsJson !", gadgetId, ARG_FAT);
			}
		}
		return frameFat;
	}

}
