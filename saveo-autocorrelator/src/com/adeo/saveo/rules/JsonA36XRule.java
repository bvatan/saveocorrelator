package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class JsonA36XRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(JsonA36XRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*action$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_OID = "$lbp$oid";

	private final String[][] JSON_RULES = {
			{"$[*][?(@.eltID=='h_client_main')].value[*].value" , "h_client_main"},
			{"$[*][?(@.eltID=='Nom_client')].value[*].value" , "Nom_client"},
			{"$[*][?(@.eltID=='prenom_client')].value[*].value" , "prenom_client"},
			{"$[*][?(@.eltID=='h_famille_client_main')].value[*].value" , "h_famille_client_main"},
			{"$[*][?(@.eltID=='h_group_client_main')].value[*].value" , "h_group_client_main"},
			{"$[*][?(@.eltID=='e_mail_client')].value[*].value" , "email"},
			{"$[*][?(@.eltID=='tel_portable_client')].value[*].value" , "portable"},
			{"$[*][?(@.eltID=='adresse1_client')].value[*].value" , "adresse1_client"},
			{"$[*][?(@.eltID=='adresse2_client')].value[*].value" , "adresse2_client"},
			{"$[*][?(@.eltID=='adresse3_client')].value[*].value" , "adresse3_client"},
			{"$[*][?(@.eltID=='adresse4_client')].value[*].value" , "adresse4_client"},
			{"$[*][?(@.eltID=='code_postal_client')].value[*].value" , "code_postal_client"},
			{"$[*][?(@.eltID=='pays_client')].value[*].value" , "pays_client"},
			{"$[*][?(@.eltID=='tel_client')].value[*].value" , "tel_client"},
			{"$[*][?(@.eltID=='tel_portable_client')].value[*].value" , "tel_portable_client"},
			{"$[*][?(@.eltID=='h_client_entreprise')].value[*].value" , "h_client_entreprise"},
			{"$[*][?(@.eltID=='titre_client')].value[*].value" , "titre_client"},
			{"$[*][?(@.eltID=='ville_client')].value[*].value" , "ville_client"},
	};

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "A36".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_OID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String masterFrameKey = "frame:" + samplerArgs.get(ARG_FRAME_EXEC_ID);
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		if (frameVars != null) {
			String encodedJson = ExtractorUtils.extractWithRegex(httpSampleResult.getResponseDataAsString(), PATTERN_ENCODED_JSON, 1);
			String json = StringEscapeUtils.unescapeHtml(encodedJson);
			
			for(String[] rule : JSON_RULES) {
				ExtractorUtils.extractFirstJsonPath(json, rule[0], frameVars, rule[1]);
			}
			frameVars.put("e_mail_client", frameVars.get("email"));
		} else {
			if (log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} !", samplerArgs.get(ARG_FRAME_EXEC_ID), samplerArgs.get(ARG_FID));
			}
		}
	}

}
