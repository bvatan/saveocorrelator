package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class XmlA79XRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(XmlA79XRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*xml$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_OID = "$lbp$oid";


	private final String[][] JSON_RULES = {
			{ "$..AF_IDTYPE", "h_current_type" },
			{ "$..AF_IDATTACHED_FILE", "h_id_img" },
	};

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String threadGroupName = JMeterContextService.getContext().getThreadGroup().getName();
		boolean condition = threadGroupName.startsWith("S04");
		condition = condition && MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches();
		condition = condition && "$$A79".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_OID));
		return condition;
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameResponseContent = httpSampleResult.getResponseDataAsString();
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String masterFrameKey = "frame:" + samplerArgs.get(ARG_FRAME_EXEC_ID);
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		if (frameVars != null) {
			String encodedJson = ExtractorUtils.extractWithRegex(frameResponseContent, PATTERN_ENCODED_JSON, 1);
			String json = StringEscapeUtils.unescapeHtml(encodedJson);
			for (String[] rule : JSON_RULES) {
				ExtractorUtils.extractFirstJsonPath(json, rule[0], frameVars, rule[1]);
			}
			frameVars.put("attachetype", frameVars.get("h_current_type"));
		} else {
			if (log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} !", samplerArgs.get(ARG_FRAME_EXEC_ID), samplerArgs.get(ARG_FID));
			}
		}
	}

}
