package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class JsonA5XRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(JsonA5XRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*action$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_OID = "$lbp$oid";

	private final String[][] JSON_RULES = {
			{"$[*][?(@.eltID=='h_Warranty_rules')].value[*].value" , "h_Warranty_rules"},
			{"$[*][?(@.eltID=='h_af_idwarranty_uw')].value[*].value" , "h_af_idwarranty_uw"},
			{"$[*][?(@.eltID=='h_id_article_main')].value[*].value" , "h_id_article_main"},
			{"$[*][?(@.eltID=='h_familleArticle1')].value[*].value" , "h_familleArticle1"},
			{"$[*][?(@.eltID=='h_familleArticle2')].value[*].value" , "h_familleArticle2"},
			{"$[*][?(@.eltID=='h_familleArticle3')].value[*].value" , "h_familleArticle3"},
			{"$[*][?(@.eltID=='h_familleArticle4')].value[*].value" , "h_familleArticle4"},
			{"$[*][?(@.eltID=='h_familleArticle5')].value[*].value" , "h_familleArticle5"},
			{"$[*][?(@.eltID=='h_fournID')].value[*].value" , "h_fournID"},
			{"$[*][?(@.eltID=='h_familleFourn')].value[*].value" , "h_familleFourn"},
			{"$[*][?(@.eltID=='h_groupFourn')].value[*].value" , "h_groupFourn"},
			{"$[*][?(@.eltID=='warranty')].value[*].value" , "warranty"},
			{"$[*][?(@.eltID=='h_pa_base')].value[*].value" , "h_pa_base"},
			{"$[*][?(@.eltID=='h_pv_ttc')].value[*].value" , "h_pv_ttc"},
			{"$[*][?(@.eltID=='h_sysdate')].value[*].value" , "h_sysdate"},
			
	};
	
	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "A5".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_OID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String masterFrameKey = "frame:" + samplerArgs.get(ARG_FRAME_EXEC_ID);
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		if (frameVars != null) {
			String encodedJson = ExtractorUtils.extractWithRegex(httpSampleResult.getResponseDataAsString(), PATTERN_ENCODED_JSON, 1);
			String json = StringEscapeUtils.unescapeHtml(encodedJson);
			
			for(String[] rule : JSON_RULES) {
				ExtractorUtils.extractFirstJsonPath(json, rule[0], frameVars, rule[1]);
			}
			frameVars.put("warranty", frameVars.get("h_af_idwarranty_uw"));
		} else {
			if (log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} !", samplerArgs.get(ARG_FRAME_EXEC_ID), samplerArgs.get(ARG_FID));
			}
		}
	}

}
