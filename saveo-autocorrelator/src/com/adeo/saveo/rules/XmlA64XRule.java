package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.jayway.jsonpath.JsonPath;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

import net.minidev.json.JSONArray;

public class XmlA64XRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(XmlA64XRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*xml$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_OID = "$lbp$oid";

	private static final String JSON_PATH_ARTICLE_TICKET = "$[*][?(@.LMRL_ARTICLE_REF=='%s')]";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "$$A64".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_OID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameResponseContent = httpSampleResult.getResponseDataAsString();
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String masterFrameKey = "frame:" + samplerArgs.get(ARG_FRAME_EXEC_ID);
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		if (frameVars != null) {
			String encodedJson = ExtractorUtils.extractWithRegex(frameResponseContent, PATTERN_ENCODED_JSON, 1);
			String json = StringEscapeUtils.unescapeHtml(encodedJson);

			String articleNo = variables.get("articleNo");

			JSONArray res = JsonPath.read(json, String.format(JSON_PATH_ARTICLE_TICKET, articleNo));
			Map<String, String> jsonMap = (Map<String, String>)res.get(0);
			frameVars.put("h_num_lig_ticket", jsonMap.get("LMRL_LINE_NO"));
			frameVars.put("h_tmp_code_recherche_client", jsonMap.get("LMR_CLIENT_NO"));
			frameVars.put("h_id_article_main_prix_vte", jsonMap.get("PRIX_HT"));
			frameVars.put("h_id_article_main_prix_vte_ttc", jsonMap.get("PRIX_TTC"));
			
//			ExtractorUtils.extractFirstJsonPath(articleJson, JSON_PATH_ARTICLE_NUM_LIG, frameVars, "h_num_lig_ticket");
//			ExtractorUtils.extractFirstJsonPath(articleJson, JSON_PATH_ARTICLE_CLIENT_NO, frameVars, "h_tmp_code_recherche_client");
//			ExtractorUtils.extractFirstJsonPath(articleJson, JSON_PATH_ARTICLE_PRIX_HT, frameVars, "h_id_article_main_prix_vte");
//			ExtractorUtils.extractFirstJsonPath(articleJson, JSON_PATH_ARTICLE_PRIX_TTC, frameVars, "h_id_article_main_prix_vte_ttc");

			correlatedVariables.put("h_num_lig_ticket", frameVars.get("h_num_lig_ticket"));
			correlatedVariables.put("h_num_lig_ticket", jsonMap.get("LMRL_LINE_NO"));
			correlatedVariables.put("h_tmp_code_recherche_client", jsonMap.get("LMR_CLIENT_NO"));
			correlatedVariables.put("h_id_article_main_prix_vte", jsonMap.get("PRIX_HT"));
			correlatedVariables.put("h_id_article_main_prix_vte_ttc", jsonMap.get("PRIX_TTC"));
		} else {
			if (log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} !", samplerArgs.get(ARG_FRAME_EXEC_ID), samplerArgs.get(ARG_FID));
			}
		}
	}

}
