package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.InjectorUtils;
import com.adeo.saveo.SamplerMonkey;
import com.adeo.saveo.ValueProcessor;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class SubFrameIRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(SubFrameIRule.class);

	private static Pattern SAMPLER_PATH_PATTERN = Pattern.compile(".*(?:action|gadget|internal|menu|xml|help|upload)$");

	private static final String ARG_OFID = "$lbp$0fid";
	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";
	private static final String ARG_EXEC_ID = "$lbp$execID";
	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_AAT = "$lbp$aat";
	private static final String ARG_LCK = "$lbp$lck";
	private static final String ARG_OID = "$lbp$oid";
	private static final String VAL_OID_UNLOCK_FRAME = "$UNLOCK_FRAME$";

	private static final Pattern PATTERN_ARG_NAME_EXEC_ID = Pattern.compile(Pattern.quote(ARG_EXEC_ID));
	private static final Pattern PATTERN_ARG_VAL_EXEC_ID = Pattern.compile("(F:.*)(t\\d+)");

//	private static final Pattern PATTERN_ARG_NAME_FRAME_EXEC_ID = Pattern.compile(Pattern.quote(ARG_FRAME_EXEC_ID));
//	private static final Pattern PATTERN_ARG_VAL_FRAME_EXEC_ID = Pattern.compile("F:.*");

	private static final Pattern PATTERN_HDR_NAME_X_LBR_AAT = Pattern.compile(Pattern.quote("x-lbr-aat"));
	private static final Pattern PATTERN_HDR_VAL_X_LBR_AAT = Pattern.compile("^.*$");

	private static final Pattern PATTERN_ARG_NAME_H_NUMERO_FACTURE_ACHAT = Pattern.compile(Pattern.quote("h_numero_facture_achat"));
	private static final Pattern PATTERN_ARG_VAL_H_NUMERO_FACTURE_ACHAT = Pattern.compile("^\\d+-\\d+(.*)");

	private static final Pattern PATTERN_ARG_NAME_LBP_LCK = Pattern.compile(Pattern.quote("$lbp$lck"));
//	private static final Pattern PATTERN_ARG_VAL_LBP_LCK_REPAIR = Pattern.compile("\\{\"a\":true,\"t\":\"[^\"]*\",\"r\":\"TB_REPAIR_INFO\",\"k\":\"\\d+\"\\}");
//	private static final Pattern PATTERN_ARG_VAL_LBP_LCK_VENTE = Pattern.compile("\\{\"a\":true,\"t\":\"[^\"]*\",\"r\":\"VENTESENTETE\",\"k\":\"\\d+\"\\}");
//	private static final Pattern PATTERN_ARG_VAL_LBP_LCK_ACHAT = Pattern.compile("\\{\"a\":true,\"t\":\"[^\"]*\",\"r\":\"ACHATSENTETE\",\"k\":\"\\d+\"\\}");

	private static final Pattern PATTERN_ARG_VAL_LBP_LCK_REPAIR = Pattern.compile("\\{\"a\":true,\"t\":\"([^\"]*)\",\"r\":\"TB_REPAIR_INFO\",\"k\":\"\\d+\"\\}");
	private static final Pattern PATTERN_ARG_VAL_LBP_LCK_VENTES = Pattern.compile("\\{\"a\":true,\"t\":\"([^\"]*)\",\"r\":\"VENTESENTETE\",\"k\":\"\\d+\"\\}");
	private static final Pattern PATTERN_ARG_VAL_LBP_LCK_ACHATS = Pattern.compile("\\{\"a\":true,\"t\":\"([^\"]*)\",\"r\":\"ACHATSENTETE\",\"k\":\"\\d+\"\\}");

	private static final Pattern PATTERN_ARG_NAME_T = Pattern.compile(Pattern.quote("t"));
	private static final Pattern PATTERN_ARG_VAL_T = Pattern.compile("^.*$");

	private static final Pattern PATTERN_ARG_NAME_K = Pattern.compile(Pattern.quote("k"));
	private static final Pattern PATTERN_ARG_VAL_K = Pattern.compile("^.*$");

	private ValueProcessor lckRepairValueProcessor = new LckRepairValueProcessor();
	private ValueProcessor lckAchatsValueProcessor = new LckAchatsValueProcessor();
	private ValueProcessor lckVentesValueProcessor = new LckVentesValueProcessor();
	private ValueProcessor unlockFrameTValueProcessor = new UnlockFrameTValueProcessor();
	private ValueProcessor unlockFrameKValueProcessor = new UnlockFrameKValueProcessor();
	private ValueProcessor aatValueProcessor = new AatValueProcessor();
	private ValueProcessor execIdValueProcessor = new ExecIdValueProcessor();

	private SamplerMonkey unlockFrameMonkey;
	private SamplerMonkey withLckParamMonkey;
	private SamplerMonkey commonRequestMonkey;;

	public SubFrameIRule() {
		unlockFrameMonkey = createMonkeyForUnlockFrameRequests();
		withLckParamMonkey = createMonkeyForLckRequests();
		commonRequestMonkey = createMonkeyForHeadersOnly();
	}

	/**
	 * Only check headers for x-lbr-aat header
	 */
	private SamplerMonkey createMonkeyForHeadersOnly() {
		SamplerMonkey monkey = new SamplerMonkey();
		monkey.addHeaderRule(PATTERN_HDR_NAME_X_LBR_AAT, PATTERN_HDR_VAL_X_LBR_AAT, aatValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_EXEC_ID, PATTERN_ARG_VAL_EXEC_ID, execIdValueProcessor);
		return monkey;
	}

	/**
	 * For requests that have $lbp$lck parameter
	 */
	private SamplerMonkey createMonkeyForLckRequests() {
		SamplerMonkey monkey = new SamplerMonkey();
		monkey.addHeaderRule(PATTERN_HDR_NAME_X_LBR_AAT, PATTERN_HDR_VAL_X_LBR_AAT, aatValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_REPAIR, lckRepairValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_VENTES, lckVentesValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_ACHATS, lckAchatsValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_EXEC_ID, PATTERN_ARG_VAL_EXEC_ID, execIdValueProcessor);
		return monkey;
	}

	/**
	 * For UNLOCK_FRAME requests
	 */
	private SamplerMonkey createMonkeyForUnlockFrameRequests() {
		SamplerMonkey monkey = new SamplerMonkey();
		monkey.addHeaderRule(PATTERN_HDR_NAME_X_LBR_AAT, PATTERN_HDR_VAL_X_LBR_AAT, aatValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_REPAIR, lckRepairValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_VENTES, lckVentesValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_ACHATS, lckAchatsValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_T, PATTERN_ARG_VAL_T, unlockFrameTValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_K, PATTERN_ARG_VAL_K, unlockFrameKValueProcessor);
		monkey.addArgumentRule(PATTERN_ARG_NAME_EXEC_ID, PATTERN_ARG_VAL_EXEC_ID, execIdValueProcessor);
		return monkey;
	}

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return SAMPLER_PATH_PATTERN.matcher(samplerBase.getPath()).matches();
	}

	@Override
	public void applyBefore(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		CorrelatedVariables masterFrameVars = (CorrelatedVariables) variables.getObject("originatingFrame:" + samplerArgs.get(ARG_FRAME_EXEC_ID));
		if (masterFrameVars == null && samplerBase.getName().endsWith("upload")) {
			masterFrameVars = (CorrelatedVariables) variables.getObject("frameId:" + samplerArgs.get(ARG_OFID));
		}
		if (masterFrameVars != null) {
//			String frameExecId = masterFrameVars.get(ARG_FRAME_EXEC_ID);
//			String aat = masterFrameVars.get(ARG_AAT);
//			String magasin = variables.get("magasin");
//			String ticketNo = variables.get("ticketNo");
//			SamplerMonkey monkey = new SamplerMonkey();
			SamplerMonkey monkey = null;
//			monkey.addHeaderRule(PATTERN_HDR_NAME_X_LBR_AAT, PATTERN_HDR_VAL_X_LBR_AAT, aatValueProcessor);
//			monkey.addHeaderRule(PATTERN_HDR_NAME_X_LBR_AAT, PATTERN_HDR_VAL_X_LBR_AAT, aat);
//			monkey.addArgumentRule(PATTERN_ARG_NAME_EXEC_ID, PATTERN_ARG_VAL_EXEC_ID, frameExecId + "$1");
//			monkey.addArgumentRule(PATTERN_ARG_NAME_FRAME_EXEC_ID, PATTERN_ARG_VAL_FRAME_EXEC_ID, frameExecId);
//			monkey.addArgumentRule(PATTERN_ARG_NAME_H_NUMERO_FACTURE_ACHAT, PATTERN_ARG_VAL_H_NUMERO_FACTURE_ACHAT, magasin + "-" + ticketNo + "$1");
			if (VAL_OID_UNLOCK_FRAME.equals(samplerArgs.get(ARG_OID))) {
//				monkey.addArgumentRule(PATTERN_ARG_NAME_T, PATTERN_ARG_VAL_T, unlockFrameTValueProcessor);
//				monkey.addArgumentRule(PATTERN_ARG_NAME_K, PATTERN_ARG_VAL_K, unlockFrameKValueProcessor);
				monkey = unlockFrameMonkey;
			} else if (samplerArgs.containsKey(ARG_LCK)) {
//				String lbpLckRepairValue = String.format("{\"a\":true,\"t\":\"%s\",\"r\":\"TB_REPAIR_INFO\",\"k\":\"%s\"}", frameExecId, masterFrameVars.get("h_siglnumlig_main"));
//				monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_REPAIR, lbpLckRepairValue);
//				String lbpLckVentesValue = String.format("{\"a\":true,\"t\":\"%s\",\"r\":\"VENTESENTETE\",\"k\":\"%s\"}", frameExecId, masterFrameVars.get("h_siglnum"));
//				monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_VENTE, lbpLckVentesValue);
//				String lbpLckAchatsValue = String.format("{\"a\":true,\"t\":\"%s\",\"r\":\"ACHATSENTETE\",\"k\":\"%s\"}", frameExecId, masterFrameVars.get("h_siglnum"));
//				monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_ACHAT, lbpLckAchatsValue);

//				monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_REPAIR, lckRepairValueProcessor);
//				monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_VENTES, lckVentesValueProcessor);
//				monkey.addArgumentRule(PATTERN_ARG_NAME_LBP_LCK, PATTERN_ARG_VAL_LBP_LCK_ACHATS, lckAchatsValueProcessor);
				monkey = withLckParamMonkey;
			} else {
				monkey = commonRequestMonkey;
			}
			monkey.runMonkey(samplerBase, variables, masterFrameVars);
			InjectorUtils.replaceInArguments(samplerBase, masterFrameVars);
		} else {
			if (log.isErrorEnabled()) {
				String frameId = samplerArgs.get(ARG_FID);
				if (frameId != null) {
					log.error("No frame variables available for frameId={} while applying before sample with path={} !", frameId, samplerBase.getPath());
				}
			}
		}
		InjectorUtils.replaceInArguments(samplerBase, correlatedVariables);
	}

	private class LckRepairValueProcessor implements ValueProcessor {
		@Override
		public String processValue(JMeterVariables variables, CorrelatedVariables frameVars, HTTPSamplerBase samplerBase, String argName, String argValue, Matcher matcher) {
			String originalFrameExecId = matcher.group(1);
			CorrelatedVariables originatingFrameVars = (CorrelatedVariables) variables.getObject("originatingFrame:" + originalFrameExecId);
			String t = originatingFrameVars == null ? "null" : originatingFrameVars.get(ARG_FRAME_EXEC_ID);
			String k = frameVars.get("h_siglnumlig_main");
			return String.format("{\"a\":true,\"t\":\"%s\",\"r\":\"TB_REPAIR_INFO\",\"k\":\"%s\"}", t, k);
		}
	}

	private class LckVentesValueProcessor implements ValueProcessor {
		@Override
		public String processValue(JMeterVariables variables, CorrelatedVariables frameVars, HTTPSamplerBase samplerBase, String argName, String argValue, Matcher matcher) {
			String originalFrameExecId = matcher.group(1);
			CorrelatedVariables originatingFrameVars = (CorrelatedVariables) variables.getObject("originatingFrame:" + originalFrameExecId);
			String t = originatingFrameVars == null ? "null" : originatingFrameVars.get(ARG_FRAME_EXEC_ID);
			String k = frameVars.get("h_siglnum");
			return String.format("{\"a\":true,\"t\":\"%s\",\"r\":\"VENTESENTETE\",\"k\":\"%s\"}", t, k);
		}
	}

	private class LckAchatsValueProcessor implements ValueProcessor {
		@Override
		public String processValue(JMeterVariables variables, CorrelatedVariables frameVars, HTTPSamplerBase samplerBase, String argName, String argValue, Matcher matcher) {
			String originalFrameExecId = matcher.group(1);
			CorrelatedVariables originatingFrameVars = (CorrelatedVariables) variables.getObject("originatingFrame:" + originalFrameExecId);
			String t = originatingFrameVars == null ? "null" : originatingFrameVars.get(ARG_FRAME_EXEC_ID);
			String k = frameVars.get("h_siglnum");
			return String.format("{\"a\":true,\"t\":\"%s\",\"r\":\"ACHATSENTETE\",\"k\":\"%s\"}", t, k);
		}
	}

	private class UnlockFrameTValueProcessor implements ValueProcessor {
		@Override
		public String processValue(JMeterVariables variables, CorrelatedVariables frameVars, HTTPSamplerBase samplerBase, String argName, String argValue, Matcher matcher) {
			String originalFrameExecId = matcher.group(0);
			CorrelatedVariables originatingFrameVars = (CorrelatedVariables) variables.getObject("originatingFrame:" + originalFrameExecId);
			if (originatingFrameVars == null) {
				return null;
			} else {
				return originatingFrameVars.get(ARG_FRAME_EXEC_ID);
			}
		}
	}

	private class UnlockFrameKValueProcessor implements ValueProcessor {
		@Override
		public String processValue(JMeterVariables variables, CorrelatedVariables frameVars, HTTPSamplerBase samplerBase, String argName, String argValue, Matcher matcher) {
			return frameVars.get("h_siglnum");
		}
	}

	private class AatValueProcessor implements ValueProcessor {
		@Override
		public String processValue(JMeterVariables variables, CorrelatedVariables frameVars, HTTPSamplerBase samplerBase, String argName, String argValue, Matcher matcher) {
			return frameVars.get(ARG_AAT);
		}
	}

	private class ExecIdValueProcessor implements ValueProcessor {
		@Override
		public String processValue(JMeterVariables variables, CorrelatedVariables frameVars, HTTPSamplerBase samplerBase, String argName, String argValue, Matcher matcher) {
			String originalFrameExecId = matcher.group(1);
			String tail = matcher.group(2);
			CorrelatedVariables originatingFrameVars = (CorrelatedVariables) variables.getObject("originatingFrame:" + originalFrameExecId);
			String frameExecId = originatingFrameVars.get(ARG_FRAME_EXEC_ID);
			return frameExecId + tail;
		}
	}

}
