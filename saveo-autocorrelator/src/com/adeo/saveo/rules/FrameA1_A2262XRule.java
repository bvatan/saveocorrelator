package com.adeo.saveo.rules;

import java.util.regex.Pattern;

import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.jsoup.Jsoup;

import com.adeo.saveo.ExtractorUtils;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class FrameA1_A2262XRule extends Rule {

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*frame$");

	private static final String ARG_FID = "$lbp$fid";
	
	private final RegexRule[] REGX_RULES = {
			new RegexRule("var _\\$LBP\\$_IN_(h_[^=]*)='([^']*)'"),
	};

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "LBS01_A1_A2262".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_FID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameIdKey = "originatingFrame:" + samplerBase.getComment();
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(frameIdKey);
		if (frameVars != null) {
			String content = httpSampleResult.getResponseDataAsString();
			for(RegexRule regexRule : REGX_RULES) {
				if(regexRule.isSingleValued()) {
					ExtractorUtils.extractWithRegex(content, regexRule.getPattern(), 1, frameVars, regexRule.getVariableName());
				} else {
					ExtractorUtils.extractMultipleWithRegex(content, regexRule.getPattern(), frameVars);
				}
			}
			Jsoup.parse(content).select("#type option:containsOwn(TRONCONNEUSE)");
		}
	}

}
