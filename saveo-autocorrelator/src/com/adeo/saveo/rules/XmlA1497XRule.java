package com.adeo.saveo.rules;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.ExtractorUtils;
import com.jayway.jsonpath.JsonPath;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

import net.minidev.json.JSONArray;

public class XmlA1497XRule extends Rule {

	private static final Logger log = LoggerFactory.getLogger(XmlA1497XRule.class);

	private static final Pattern PATTERN_ENCODED_JSON = Pattern.compile("<lbr:json-response>(.*)</lbr:json-response>");

	private static final Pattern MATCH_PATH_PATTERN = Pattern.compile(".*xml$");

	private static final String ARG_FRAME_EXEC_ID = "$lbp$fExecID";

	private static final String ARG_FID = "$lbp$fid";
	private static final String ARG_OID = "$lbp$oid";

	private static final String JSON_PATH = "$[*][0]";

	@Override
	public boolean matches(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		return MATCH_PATH_PATTERN.matcher(samplerBase.getPath()).matches() && "$$A1497".equals(samplerBase.getArguments().getArgumentsAsMap().get(ARG_OID));
	}

	@Override
	public void applyAfter(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		String frameResponseContent = httpSampleResult.getResponseDataAsString();
		Map<String, String> samplerArgs = samplerBase.getArguments().getArgumentsAsMap();
		String masterFrameKey = "frame:" + samplerArgs.get(ARG_FRAME_EXEC_ID);
		CorrelatedVariables frameVars = (CorrelatedVariables) variables.getObject(masterFrameKey);
		if (frameVars != null) {
			String encodedJson = ExtractorUtils.extractWithRegex(frameResponseContent, PATTERN_ENCODED_JSON, 1);
			String json = StringEscapeUtils.unescapeHtml(encodedJson);

			JSONArray res = JsonPath.read(json, JSON_PATH);
			if(!res.isEmpty()) {
				Map<String, String> jsonMap = (Map<String, String>)res.get(0);
//				frameVars.put("h_sigle", jsonMap.get("ID_SIGLE"));
				frameVars.put("h_siglnum", jsonMap.get("ID_SIGLNUM"));
				frameVars.put("h_siglnumlig", jsonMap.get("ID_SIGLNUMLIG"));
				String threadGroupName = JMeterContextService.getContext().getThreadGroup().getName();
				if(threadGroupName.startsWith("S04") || threadGroupName.startsWith("S10")) {
					frameVars.put("h_activity", jsonMap.get("ID_ACTIVITY"));
				}
			}
		} else {
			if (log.isErrorEnabled()) {
				log.error("No frame variables for execId={} and frameId={} !", samplerArgs.get(ARG_FRAME_EXEC_ID), samplerArgs.get(ARG_FID));
			}
		}
	}

}
