package com.adeo.saveo;

import java.util.regex.Matcher;

import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;

import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public interface ValueProcessor {

	String processValue(JMeterVariables variables, CorrelatedVariables correlatedVariables, HTTPSamplerBase samplerBase, String argName, String argValue, Matcher matcher);

}
