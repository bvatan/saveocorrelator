package com.adeo.saveo;

import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class SaveoFrame extends CorrelatedVariables {

	private SaveoFrame parentFrame = null;
	
	public void setParentFrame(SaveoFrame saveoFrame) {
		parentFrame = saveoFrame;
	}

	@Override
	public String get(String key, String defaultValue) {
		String out = super.get(key, null);
		if(out == null) {
			if(parentFrame != null) {
				out = parentFrame.get(key);
				if(out != null) {
					return out;
				}
			}
			return defaultValue;
		} else {
			return out;
		}
	}
	
	@Override
	public String get(String name) {
		return get(name, null);
	}
}
