package com.adeo.saveo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

import net.minidev.json.JSONArray;

public class ExtractorUtils extends XIUtils {

	private static final Logger log = LoggerFactory.getLogger(ExtractorUtils.class);

	/**
	 * Extrait un attribut en utilisant JSOUP
	 * 
	 * @param document            le document généré à partir de la réponse HTML
	 * @param selector            le selecteur JSOUP
	 * @param attribute           le nom de l'attribut à extraire, si vide, on prend
	 *                            le texte
	 * @param position            le n° de la réponse à extraire (0 = aléatoire, n =
	 *                            n eme réponse)
	 * @param correlatedVariables les variables corrélées
	 * @param varName             le nom de la variable à mettre dans les variables
	 *                            corrélées
	 */
	public static void extractAttributeWithCssQuery(Document document, String selector, int position, String attribute, CorrelatedVariables correlatedVariables, String varName) {
		Elements elements = document.select(selector);
		if (elements.size() > position) {
			Element element = elements.get(position);
			String value = null;
			if (!StringUtils.isEmpty(attribute)) {
				value = element.attr(attribute);
			} else {
				value = element.text();
			}
			if (value != null) {
				log.debug(PRINT_CORRELATED_VAR, varName, value);
				correlatedVariables.put(varName, value);
			}
		}
	}

	/**
	 * Extrait une valeur en fonction d'une regex
	 * 
	 * @param content             la réponse de la requête
	 * @param pattern             la regex compilée
	 * @param position            le n° du groupe qui match la regex (0 = réponse
	 *                            entiere, n = n eme groupe capturant)
	 * @param correlatedVariables les variables corrélées
	 * @param varName             le nom de la variable à mettre dans les variables
	 *                            corrélées
	 */
	public static void extractWithRegex(String content, Pattern pattern, int position, CorrelatedVariables correlatedVariables, String varName) {
		Matcher matcher = pattern.matcher(content);
		if (matcher.find() && matcher.groupCount() >= position) {
			String value = matcher.group(position);
			if (value != null) {
				log.debug(PRINT_CORRELATED_VAR, varName, value);
				correlatedVariables.put(varName, value);
			}
		}
	}
	
	public static String extractWithRegex(String content, Pattern pattern, int position) {
		Matcher matcher = pattern.matcher(content);
		if (matcher.find() && matcher.groupCount() >= position) {
			return matcher.group(position);
		}
		return null;
	}

	/**
	 * Extrait plusieurs valeurs en fonction d'une regex
	 * 
	 * @param content             la réponse de la requête
	 * @param pattern             la regex compilée
	 * @param correlatedVariables les variables corrélées
	 */
	public static void extractMultipleWithRegex(String content, Pattern pattern, CorrelatedVariables correlatedVariables) {
		Matcher matcher = pattern.matcher(content);
		while (matcher.find()) {
			for (int i = 1; i < matcher.groupCount(); i++) {
				if (log.isDebugEnabled()) {
					log.debug(PRINT_CORRELATED_VAR, matcher.group(i), matcher.group(i + 1));
				}
				correlatedVariables.put(matcher.group(i), matcher.group(i + 1));
			}
		}
	}
	
	public static String extractFirstJsonPath(String json, String jsonPath) {
		JSONArray res = JsonPath.read(json, jsonPath);
		if(res.size() > 0) {
			return res.get(0).toString();
		}
		return null;
	}
	
	public static void extractFirstJsonPath(String json, String jsonPath, CorrelatedVariables variables, String key) {
//		JSONArray res = JsonPath.read(json, jsonPath);
		Object oRes = JsonPath.read(json, jsonPath);
		if(oRes instanceof JSONArray) {
			JSONArray res = (JSONArray)oRes;
			if(res.size() > 0) {
				String value = res.get(0).toString();
				if (log.isDebugEnabled()) {
					log.debug(PRINT_CORRELATED_VAR, key, value);
				}
				variables.put(key, value);
			}
		} else if(oRes instanceof String) {
			if (log.isDebugEnabled()) {
				log.debug(PRINT_CORRELATED_VAR, key, oRes);
			}
			variables.put(key, oRes.toString());
		}
	}

	/**
	 * Transforme la réponse en objet JSON, et extrait la valeur associée à la clef
	 * 
	 * @param content             la réponse JSON
	 * @param correlatedVariables les variables corrélées
	 * @param key                 la clef a extraire
	 */
	public static void extractJSON(String content, CorrelatedVariables correlatedVariables, String key) {
		JSONObject jsonObject = new JSONObject(content);
		putStringInCorrelatedVariables(jsonObject, correlatedVariables, key, key);
		putIntInCorrelatedVariables(jsonObject, correlatedVariables, key);
	}

	/**
	 * Lors de la 1er réponse JSON pour Vaadin, toutes les informations se trouvent
	 * dans un champ qui a pour clef "uidl" Ce champ est un JSON stringifié, il faut
	 * donc d'abord extraire la String, la transformer en objet JSON, puis enfin
	 * effectuer les extractions
	 * 
	 * @param content             la réponse HTML
	 * @param correlatedVariables les variables corrélées
	 */
	public static void extractJSONUidl(String content, CorrelatedVariables correlatedVariables) {
		String defaultValue = "NO-VALUE";
		JSONObject jsonObject = new JSONObject(content);
		String uidl = jsonObject.optString("uidl", defaultValue);
		if (!defaultValue.equals(uidl)) {
			JSONObject jsonUidl = new JSONObject(uidl);
			putStringInCorrelatedVariables(jsonUidl, correlatedVariables, VAADIN_SECURITY_KEY, CSRF_TOKEN);
			putIntInCorrelatedVariables(jsonUidl, correlatedVariables, CLIENT_ID);
			putIntInCorrelatedVariables(jsonUidl, correlatedVariables, SYNC_ID);
		}
	}

	/**
	 * Recupere la valeur associée à la clef dans l'object JSON, et si elle existe,
	 * la met dans les correlatedVariables Si elle n'existe pas, on sort simplement
	 * de la méthode Cette méthode itère sur toutes les clefs JSON
	 * 
	 * Ici, on est obligé de préciser la clef à mettre dans les variables corrélées.
	 * En effet, pour CrsfToken, la valeur à extraire est Vaadin-Security-Key, mais
	 * celle à injecter est CrsfToken; il faut donc préciser les 2
	 * 
	 * @param jsonObject          l'objet JSON contenant les valeurs à extraire
	 * @param correlatedVariables les variables à corréler
	 * @param key                 la clef dont la valeur est à extraire
	 * @param correlatedKey       la clef à rentrer dans les variables corrélées
	 */
	private static void putStringInCorrelatedVariables(JSONObject jsonObject, CorrelatedVariables correlatedVariables, String key, String correlatedKey) {
		String defaultValue = "NO-VALUE";
		String value = jsonObject.optString(key, defaultValue);
		if (!defaultValue.equals(value)) {
			correlatedVariables.put(correlatedKey, value);
			log.debug(PRINT_CORRELATED_VAR, correlatedKey, value);
		}
	}

	/**
	 * Recupere la valeur associée à la clef dans l'object JSON, et si elle existe,
	 * la met dans les correlatedVariables Si elle n'existe pas, on sort simplement
	 * de la méthode Cette méthode itère sur tous les clefs JSON
	 * 
	 * @param jsonObject          l'objet JSON contenant les valeurs à extraire
	 * @param correlatedVariables les variables à corréler
	 * @param key                 la clef dont la valeur est à extraire
	 */
	private static void putIntInCorrelatedVariables(JSONObject jsonObject, CorrelatedVariables correlatedVariables, String key) {
		int defaultValue = -165918;
		int value = jsonObject.optInt(key, defaultValue);
		if (value != defaultValue) {
			correlatedVariables.put(key, String.valueOf(value));
			log.debug(PRINT_CORRELATED_VAR, key, value);
		}
	}

}
