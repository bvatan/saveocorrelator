package com.adeo.saveo;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.http.control.Header;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.protocol.http.util.HTTPArgument;
import org.apache.jmeter.testelement.property.CollectionProperty;
import org.apache.jmeter.testelement.property.JMeterProperty;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class SamplerMonkey {

	private static final Logger log = LoggerFactory.getLogger(SamplerMonkey.class);

	private List<ReplaceRule> argumentsRules = new LinkedList<>();
	private List<ReplaceRule> headersRules = new LinkedList<>();

	public void addArgumentRule(Pattern argPattern, Pattern valPattern, ValueProcessor processor) {
		argumentsRules.add(new ReplaceRule(argPattern, valPattern, processor));
	}

	public void addHeaderRule(Pattern argPattern, Pattern valPattern, ValueProcessor processor) {
		headersRules.add(new ReplaceRule(argPattern, valPattern, processor));
	}
	
	public void addArgumentRule(Pattern argPattern, Pattern valPattern, String replacement) {
		argumentsRules.add(new ReplaceRule(argPattern, valPattern, replacement));
	}

	public void addHeaderRule(Pattern argPattern, Pattern valPattern, String replacement) {
		headersRules.add(new ReplaceRule(argPattern, valPattern, replacement));
	}

	public void runMonkey(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		applyHeadersRules(samplerBase, variables, correlatedVariables);
		applyArgumentsRules(samplerBase, variables, correlatedVariables);
	}

	private void applyHeadersRules(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		if (samplerBase.getHeaderManager() == null) {
			return;
		}
		CollectionProperty headers = samplerBase.getHeaderManager().getHeaders();
		for (JMeterProperty headerProperty : headers) {
			Header header = (Header) headerProperty.getObjectValue();
			String headerName = header.getName();
			ReplaceRule ruleApplied = null;
			String newValue = null;
			for (ReplaceRule rule : headersRules) {
				newValue = rule.getNewValue(samplerBase, headerName, header.getValue(), variables, correlatedVariables);
				if (newValue != null) {
					ruleApplied = rule;
					break;
				}
			}
			if (ruleApplied != null) {
				if (log.isDebugEnabled()) {
					log.debug("Set header {}=<{}> old value was {} in sample {}", headerName, newValue, header.getValue(), samplerBase.getName());
				}
				header.setValue(newValue);
			}
//			String correlatedValue = correlatedVariables.get(header.getName());
//			if (correlatedValue != null) {
//				header.setValue(correlatedValue);
//				if (log.isDebugEnabled()) {
//					log.debug("Autocorrelator replacing Header {} from value {} => {}", header.getName(), header.getValue(), correlatedValue);
//				}
//			}
//			if (header.getName().toLowerCase().equalsIgnoreCase(HEADER_REFERER)) {
//				String newValue = replaceInPath(header.getValue(), correlatedVariables);
//				header.setValue(newValue);
//			}
		}
	}

	private void applyArgumentsRules(HTTPSamplerBase samplerBase, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
		Arguments args = samplerBase.getArguments();
		Arguments newArgs = new Arguments();
		samplerBase.setArguments(newArgs);
		for (JMeterProperty argument : args) {
			HTTPArgument httpArg = (HTTPArgument) argument.getObjectValue();
			String argName = httpArg.getName();
			ReplaceRule ruleApplied = null;
			String newValue = null;
			for (ReplaceRule rule : argumentsRules) {
				newValue = rule.getNewValue(samplerBase, argName, httpArg.getValue(), variables, correlatedVariables);
				if (newValue != null) {
					ruleApplied = rule;
					break;
				}
			}
			if (ruleApplied != null) {
				newArgs.addArgument(new HTTPArgument(httpArg.getName(), newValue, httpArg.getMetaData()));
				if (log.isDebugEnabled()) {
					log.debug("Set argument {}=<{}> old value was {} in sample {}", argName, newValue, httpArg.getValue(), samplerBase.getName());
				}
			} else {
				newArgs.addArgument(httpArg);
			}
		}
	}

	class ReplaceRule {
		private Pattern argPattern;
		private Pattern valPattern;
		private String replacement;
		private ValueProcessor processor;

		ReplaceRule(Pattern argPattern, Pattern valPattern, String replacement) {
			this.argPattern = argPattern;
			this.valPattern = valPattern;
			this.replacement = replacement;
		}
		
		ReplaceRule(Pattern argPattern, Pattern valPattern, ValueProcessor processor) {
			this.argPattern = argPattern;
			this.valPattern = valPattern;
			this.processor = processor;
		}

		String getId() {
			return argPattern.pattern() + "=" + valPattern.toString() + "/" + replacement;
		}

		boolean targetsArgument(String argName) {
			return argPattern.matcher(argName).matches();
		}

		String getNewValue(HTTPSamplerBase samplerBase, String argName, String argValue, JMeterVariables variables, CorrelatedVariables correlatedVariables) {
			if (targetsArgument(argName)) {
				Matcher matcher = valPattern.matcher(argValue);
				if (matcher.matches()) {
					if(processor == null) {
						return matcher.replaceAll(replacement);
					} else {
						try {
							return processor.processValue(variables, correlatedVariables, samplerBase, argName, argValue, matcher);
						} catch(Exception e) {
							if(log.isErrorEnabled()) {
								log.error("SamplerMonkey: ValueProcessor.processValue() failed with message : {}", e.getMessage(), e);
							}
							return null;
						}
					}
				} else {
					return null;
				}
			} else {
				return null;
			}
		}
	}

}
