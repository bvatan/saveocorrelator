package com.adeo.saveo;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.http.control.Cookie;
import org.apache.jmeter.protocol.http.control.Header;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.protocol.http.util.HTTPArgument;
import org.apache.jmeter.testelement.property.CollectionProperty;
import org.apache.jmeter.testelement.property.JMeterProperty;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class InjectorUtils extends XIUtils {

	private static final Logger log = LoggerFactory.getLogger(InjectorUtils.class);

	/**
	 * Remplace les variables corrélées dans les headers
	 * 
	 * @param httpSamplerBase     la requête HTTP
	 * @param correlatedVariables les variables corrélées
	 */
	public static void replaceInHeaders(HTTPSamplerBase httpSamplerBase, CorrelatedVariables correlatedVariables) {
		if (httpSamplerBase.getHeaderManager() != null) {
			CollectionProperty headers = httpSamplerBase.getHeaderManager().getHeaders();
			for (JMeterProperty property : headers) {
				Header header = (Header) property.getObjectValue();
				String correlatedValue = correlatedVariables.get(header.getName());
				if (correlatedValue != null) {
					header.setValue(correlatedValue);
					if (log.isDebugEnabled()) {
						log.debug("Autocorrelator replacing Header {} from value {} => {}", header.getName(), header.getValue(), correlatedValue);
					}
				}
				if (header.getName().toLowerCase().equalsIgnoreCase(HEADER_REFERER)) {
					String newValue = replaceInPath(header.getValue(), correlatedVariables);
					header.setValue(newValue);
				}
			}
		}
	}

	/**
	 * Remplace les variables corrélées dans les headers
	 * 
	 * @param httpSamplerBase     la requête HTTP
	 * @param correlatedVariables les variables corrélées
	 */
	public static void replaceInCookieHeader(HTTPSamplerBase httpSamplerBase, CorrelatedVariables correlatedVariables, Pattern pattern, String correlatedVariable) {
		if (httpSamplerBase.getHeaderManager() != null) {
			CollectionProperty headers = httpSamplerBase.getHeaderManager().getHeaders();
			for (JMeterProperty property : headers) {
				Header header = (Header) property.getObjectValue();
				if (header.getName().toLowerCase().equalsIgnoreCase(HEADER_COOKIE)) {
					String correlatedValue = correlatedVariables.get(correlatedVariable);
					Matcher matcher = pattern.matcher(header.getValue());
					if (correlatedValue != null && matcher.find()) {
						header.setValue(header.getValue().replaceAll(matcher.group(1), correlatedValue));
						if (log.isDebugEnabled()) {
							log.debug("Header {} : changing value with {}", header.getName(), correlatedValue);
						}
					}
				}
			}
		}
	}

	/**
	 * Remplace les variables corrélées dans les cookies
	 * 
	 * @param httpSamplerBase     la requête HTTP
	 * @param correlatedVariables les variables corrélées
	 */
	public static void replaceInCookies(HTTPSamplerBase httpSamplerBase, CorrelatedVariables correlatedVariables) {
		if (httpSamplerBase.getCookieManager() != null) {
			CollectionProperty cookies = httpSamplerBase.getCookieManager().getCookies();
			for (JMeterProperty property : cookies) {
				Cookie cookie = (Cookie) property.getObjectValue();
				String correlatedValue = correlatedVariables.get(cookie.getName());
				if (correlatedValue != null) {
					cookie.setValue(correlatedValue);
					if (log.isDebugEnabled()) {
						log.debug("Cookie {} : changing value with {}", cookie.getName(), correlatedValue);

					}
				}
			}
		}
	}

	/**
	 * Remplace les variables corrélées dans le path
	 * 
	 * @param originalPath        le path
	 * @param correlatedVariables les variables corrélées
	 * @return le path modifié, ou le path s'il n'y a pas eu de chgt
	 */
	public static String replaceInPath(String originalPath, CorrelatedVariables correlatedVariables) {
		int indexOfParams = originalPath.indexOf('?');
		// On regarde aussi s'il y a un égal, sinon on le rajoute pour rien
		int indexOfEqual = originalPath.indexOf('=');
		if (indexOfParams >= 0 && indexOfEqual >= 0) {
			String path = originalPath.substring(indexOfParams + 1);
			String[] paramAndValues = path.split("\\&");
			StringBuilder builder = new StringBuilder(path.length());
			for (int i = 0; i < paramAndValues.length; i++) {
				String[] keyValue = paramAndValues[i].split("=");
				String newValue = correlatedVariables.get(keyValue[0]);
				builder.append(keyValue[0]).append("=");
				if (newValue != null) {
					builder.append(newValue);
					log.debug("Changing arg {} in path, new value : {}", keyValue[0], newValue);
				} else {
					if (keyValue.length > 1) {
						builder.append(keyValue[1]);
					}
				}
				if (i < paramAndValues.length - 1) {
					builder.append('&');
				}
			}
			String newPath = originalPath.substring(0, indexOfParams + 1) + builder.toString();
			if (log.isDebugEnabled()) {
				log.debug("Autocorrelator replacing URL path from {} => {}", originalPath, newPath);
			}
			return newPath;
		}
		return originalPath;
	}

	/**
	 * Méthode qui modifie les paramètres de la requête HTTP
	 * 
	 * @param samplerBase         la requête HTTP
	 * @param correlatedVariables les variables corrélées
	 */
	public static void replaceInArguments(HTTPSamplerBase samplerBase, CorrelatedVariables correlatedVariables) {
		boolean replacementNeeded = false;
		Arguments arguments = samplerBase.getArguments();
		Map<String, String> argumentsAsMap = arguments.getArgumentsAsMap();
		for (String argumentName : argumentsAsMap.keySet()) {
			String correlatedValue = correlatedVariables.get(argumentName);
			if (correlatedValue != null) {
				replacementNeeded = true;
				break;
			}
		}
		if (replacementNeeded) {
			samplerBase.setArguments(new Arguments());
			for (JMeterProperty argument : arguments) {
				HTTPArgument httpArg = (HTTPArgument) argument.getObjectValue();
				String correlatedValue = correlatedVariables.get(argument.getName());
				if (correlatedValue != null) {
					samplerBase.getArguments().addArgument(new HTTPArgument(httpArg.getName(), correlatedValue, httpArg.getMetaData()));
					if(log.isTraceEnabled()) {
						log.trace("Replacement occured for argument {}=<{}>, old value was <{}> in sample {}", httpArg.getName(), correlatedValue, httpArg.getValue(), samplerBase.getName());
					}
				} else {
					samplerBase.getArguments().addArgument(httpArg);
					if(log.isTraceEnabled()) {
						log.trace("No replacement occured for argument {}=<{}> in sample {}", httpArg.getName(), httpArg.getValue(), samplerBase.getName());
					}
				}
			}
		}
		if (log.isDebugEnabled()) {
			if (replacementNeeded) {
				log.debug("Autocorrelator Replacement occured for sampler {}, initialArgs:{}, new Args:{}", samplerBase.getName(), arguments, samplerBase.getArguments());
			} else {
				log.debug("No replacement occured for sampler {}", samplerBase.getName());
			}
		}
	}
	
	public static void replaceArgument(HTTPSamplerBase samplerBase, String argName, Pattern argValuePattern, String replacement) {
		boolean replacementNeeded = false;
		Arguments arguments = samplerBase.getArguments();
		Map<String, String> argumentsAsMap = arguments.getArgumentsAsMap();
		String argValue = argumentsAsMap.get(argName);
		Matcher matcher = argValuePattern.matcher(argValue);
		if(matcher.matches()) {
			samplerBase.setArguments(new Arguments());
			for (JMeterProperty argument : arguments) {
				HTTPArgument httpArg = (HTTPArgument) argument.getObjectValue();
				if(argument.getName().equals(argName)) {
					String newArgValue = httpArg.getValue();
					samplerBase.getArguments().addArgument(new HTTPArgument(httpArg.getName(), newArgValue, httpArg.getMetaData()));
				} else {
					samplerBase.getArguments().addArgument(httpArg);
				}
			}
		}
		if (log.isDebugEnabled()) {
			if (replacementNeeded) {
				log.debug("Autocorrelator Replacement occured for sampler {}, initialArgs:{}, new Args:{}", samplerBase.getName(), arguments, samplerBase.getArguments());
			} else {
				log.debug("No replacement occured for sampler {}", samplerBase.getName());
			}
		}
	}

	/**
	 * Méthode modifiant les paramètres de la requête si ceux-ci se trouvent dans le
	 * body
	 * 
	 * @param samplerBase         la requête HTTP
	 * @param correlatedVariables les variables corrélées
	 */
	public static void replaceInJsonBodyData(HTTPSamplerBase samplerBase, CorrelatedVariables correlatedVariables) {
		JSONObject jsonObject = null;
		boolean hasChanged = false;
		Arguments arguments = samplerBase.getArguments();
		Map<String, String> argumentsAsMap = arguments.getArgumentsAsMap();
		// Dans le cas où les args sont dans body data, il n'y a qu'une entrée avec key
		// = ""
		if (argumentsAsMap.size() == 1) {
			jsonObject = new JSONObject(argumentsAsMap.get(""));
			Iterator<?> keys = jsonObject.keys();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				String correlatedValue = correlatedVariables.get(key);
				if (correlatedValue != null) {
					hasChanged = true;
					if (key.equals(SYNC_ID) || key.equals(CLIENT_ID)) {
						jsonObject.put(key, Integer.valueOf(correlatedValue));
					} else {
						jsonObject.put(key, correlatedValue);
					}
				}
			}
			if (hasChanged) {
				samplerBase.setArguments(new Arguments());
				for (JMeterProperty argument : arguments) {
					HTTPArgument httpArg = (HTTPArgument) argument.getObjectValue();
					// Il faut ajouter les arguments non encodés, sinon JMeter va les encoder, hors
					// on ne veut pas
					samplerBase.addNonEncodedArgument(httpArg.getName(), jsonObject.toString(), httpArg.getMetaData());
				}
			}
			if (log.isDebugEnabled()) {
				if (hasChanged) {
					log.debug("Replacement occured for sampler {}, initialArgs:{}, new Args:{}", samplerBase.getName(), arguments, samplerBase.getArguments());
				} else {
					log.debug("No replacement occured for sampler {}", samplerBase.getName());
				}
			}
		}
	}

	/**
	 * Methode qui remplace une partie du path suivant un pattern
	 * 
	 * @param path                le path de la requête
	 * @param pattern             la regex compilée
	 * @param key                 la clef de la valeur dans les variables corrélées
	 * @param correlatedVariables les variables corrélées
	 * @return le path modifié, ou le path s'il n'y a pas eu de chgt
	 */
	public static String replaceRegexInPath(String path, Pattern pattern, String key, CorrelatedVariables correlatedVariables) {
		Matcher matcher = pattern.matcher(path);
		String newPath = path;
		if (matcher.find()) {
			String value = correlatedVariables.get(key);
			if (value != null) {
				newPath = newPath.replaceAll(matcher.group(1), value);
				log.debug("Replacing the value of {} in path with {}", key, value);
			}
		}
		return newPath;
	}

}
