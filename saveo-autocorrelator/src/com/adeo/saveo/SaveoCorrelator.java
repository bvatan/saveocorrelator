package com.adeo.saveo;

import java.util.ArrayList;
import java.util.List;

import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.threads.JMeterVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adeo.saveo.rules.FrameA1_A2131XRule;
import com.adeo.saveo.rules.FrameA1_A2146XRule;
import com.adeo.saveo.rules.FrameA1_A2262XRule;
import com.adeo.saveo.rules.FrameA1_A2408XRule;
import com.adeo.saveo.rules.FrameA1_A2469XRule;
import com.adeo.saveo.rules.FrameA1_A2512XRule;
import com.adeo.saveo.rules.FrameIRule;
import com.adeo.saveo.rules.FrameXRule;
import com.adeo.saveo.rules.GadgetXRule;
import com.adeo.saveo.rules.JsonA1039XRule;
import com.adeo.saveo.rules.JsonA1077XRule;
import com.adeo.saveo.rules.JsonA1251XRule;
import com.adeo.saveo.rules.JsonA1252XRule;
import com.adeo.saveo.rules.JsonA125XRule;
import com.adeo.saveo.rules.JsonA139XRule;
import com.adeo.saveo.rules.JsonA1633XRule;
import com.adeo.saveo.rules.JsonA183XRule;
import com.adeo.saveo.rules.JsonA186XRule;
import com.adeo.saveo.rules.JsonA194XRule;
import com.adeo.saveo.rules.JsonA2037XRule;
import com.adeo.saveo.rules.JsonA209XRule;
import com.adeo.saveo.rules.JsonA214XRule;
import com.adeo.saveo.rules.JsonA2726XRule;
import com.adeo.saveo.rules.JsonA2777XRule;
import com.adeo.saveo.rules.JsonA2779XRule;
import com.adeo.saveo.rules.JsonA2780XRule;
import com.adeo.saveo.rules.JsonA2795XRule;
import com.adeo.saveo.rules.JsonA280XRule;
import com.adeo.saveo.rules.JsonA283XRule;
import com.adeo.saveo.rules.JsonA2842IRule;
import com.adeo.saveo.rules.JsonA314XRule;
import com.adeo.saveo.rules.JsonA329XRule;
import com.adeo.saveo.rules.JsonA338XRule;
import com.adeo.saveo.rules.JsonA33XRule;
import com.adeo.saveo.rules.JsonA36XRule;
import com.adeo.saveo.rules.JsonA52XRule;
import com.adeo.saveo.rules.JsonA562XRule;
import com.adeo.saveo.rules.JsonA5XRule;
import com.adeo.saveo.rules.JsonA60XRule;
import com.adeo.saveo.rules.JsonA60XRuleS10;
import com.adeo.saveo.rules.JsonA64XRule;
import com.adeo.saveo.rules.JsonA688XRule;
import com.adeo.saveo.rules.JsonA8XRule;
import com.adeo.saveo.rules.JsonA9XRule;
import com.adeo.saveo.rules.MenuA2411XRule;
import com.adeo.saveo.rules.Rule;
import com.adeo.saveo.rules.SubFrameIRule;
import com.adeo.saveo.rules.XmlA1497XRule;
import com.adeo.saveo.rules.XmlA40XRule;
import com.adeo.saveo.rules.XmlA64XRule;
import com.adeo.saveo.rules.XmlA79XRule;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.AbstractTechnologyCorrelator;
import com.ubikingenierie.jmeter.plugin.autocorrelator.utils.CorrelatedVariables;

public class SaveoCorrelator extends AbstractTechnologyCorrelator {

	private static final Logger log = LoggerFactory.getLogger(SaveoCorrelator.class);

	private static final String SAVEO_TECHNOLOGY = "SAVEO";

	private List<Rule> xRules = new ArrayList<>();
	private List<Rule> iRules = new ArrayList<>();

	public SaveoCorrelator() {
		// xRules
		xRules.add(new FrameXRule());
		xRules.add(new FrameA1_A2131XRule());
		xRules.add(new FrameA1_A2146XRule());
		xRules.add(new FrameA1_A2469XRule());
		xRules.add(new FrameA1_A2408XRule());
		xRules.add(new FrameA1_A2512XRule());
		xRules.add(new FrameA1_A2262XRule());
//		xRules.add(new FrameA1_A187XRule());
		xRules.add(new GadgetXRule());
		xRules.add(new XmlA40XRule());
		xRules.add(new XmlA64XRule());
		xRules.add(new XmlA1497XRule());
		xRules.add(new XmlA79XRule());
		xRules.add(new JsonA36XRule());
		xRules.add(new JsonA60XRule());
		xRules.add(new JsonA1077XRule());
		xRules.add(new JsonA2726XRule());
		xRules.add(new JsonA562XRule());
		xRules.add(new JsonA280XRule());
		xRules.add(new MenuA2411XRule());
		xRules.add(new JsonA64XRule());
		xRules.add(new JsonA186XRule());
		xRules.add(new JsonA209XRule());
		xRules.add(new JsonA2037XRule());
		xRules.add(new JsonA2777XRule());
		xRules.add(new JsonA2779XRule());
		xRules.add(new JsonA183XRule());
		xRules.add(new JsonA2780XRule());
		xRules.add(new JsonA8XRule());
		xRules.add(new JsonA5XRule());
		xRules.add(new JsonA338XRule());
		xRules.add(new JsonA329XRule());
		xRules.add(new JsonA214XRule());
		xRules.add(new JsonA283XRule());
		xRules.add(new JsonA2795XRule());
		xRules.add(new JsonA139XRule());
		xRules.add(new JsonA9XRule());
		xRules.add(new JsonA125XRule());
		xRules.add(new JsonA1039XRule());
		xRules.add(new JsonA688XRule());
		xRules.add(new JsonA52XRule());
		xRules.add(new JsonA33XRule());
		xRules.add(new JsonA314XRule());
		xRules.add(new JsonA1251XRule());
		xRules.add(new JsonA1252XRule());
		xRules.add(new JsonA194XRule());
		xRules.add(new JsonA1633XRule());
		xRules.add(new JsonA60XRuleS10());

		// iRules
		iRules.add(new FrameIRule());
		iRules.add(new SubFrameIRule());
		iRules.add(new JsonA2842IRule());
	}

	public String getId() {
		return SAVEO_TECHNOLOGY;
	}

	public void doExtraction(HTTPSampleResult httpSampleResult, HTTPSamplerBase samplerBase, JMeterVariables variables) {
		CorrelatedVariables correlatedVariables = getCorrelatedVariables(variables);
		if (log.isDebugEnabled()) {
			log.debug("********** After {}", samplerBase.getName());
		}
		for (Rule xRule : xRules) {
			if (xRule.matches(samplerBase, variables, correlatedVariables)) {
				if (log.isDebugEnabled()) {
					log.debug("Applying after rule {} for sampler {}", xRule.getId(), getSamplerDisplay(samplerBase));
				}
				try {
					xRule.applyAfter(httpSampleResult, samplerBase, variables, correlatedVariables);
				} catch(Exception e) {
					if(log.isErrorEnabled()) {
						log.error("After rule {} failed withe message {} !", xRule.getClass().getName(), e.getMessage(), e);
					}
				}
			}
		}
	}

	public void doInjection(HTTPSamplerBase samplerBase, JMeterVariables variables) {
		CorrelatedVariables correlatedVariables = getCorrelatedVariables(variables);
		if (log.isDebugEnabled()) {
			log.debug("********** Before {}", samplerBase.getName());
		}
		for (Rule iRule : iRules) {
			if (iRule.matches(samplerBase, variables, correlatedVariables)) {
				if (log.isDebugEnabled()) {
					log.debug("Applying before rule {} for sampler {}", iRule.getId(), getSamplerDisplay(samplerBase));
				}
				try {
					iRule.applyBefore(samplerBase, variables, correlatedVariables);
				} catch(Exception e) {
					if(log.isErrorEnabled()) {
						log.error("Before rule {} failed withe message {} !", iRule.getClass().getName(), e.getMessage(), e);
					}
				}
			}
		}
	}

	private String getSamplerDisplay(HTTPSamplerBase samplerBase) {
		String oid = samplerBase.getArguments().getArgumentsAsMap().get("$lbp$oid");
		return samplerBase.getName() + (oid == null ? "" : "/" + oid);
	}

}
