package com.adeo.saveo;

public class XIUtils {
	
	protected static final String HEADER_COOKIE = "Cookie";

	protected static final String PRINT_CORRELATED_VAR = "Extracted correlated var ({},{})";
    
	protected static final String VAADIN_SECURITY_KEY = "Vaadin-Security-Key";
	
	protected static final String ULP_AUTO_CORRELATOR_VARS = "ULP_AUTO_CORRELATOR_VARS";
	
    protected static final String HEADER_REFERER = "referer";
    
	protected static final String SYNC_ID = "syncId";

	protected static final String CSRF_TOKEN = "csrfToken";
	
	protected static final String CLIENT_ID = "clientId";
}
